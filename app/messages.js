const express = require('express');
const fileDb = require('../fs')

const router = express.Router();


router.get('/', (req, res) => {
    const messages = fileDb.getItems();
    res.send(messages);
});

router.post('/', (req, res) => {
    fileDb.addItem(req.body);
    res.send(req.body);
});

module.exports = router