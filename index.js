const express = require('express');
const fileDb = require('./fs');
const messages = require('./app/messages');

const app = express();
app.use(express.json());

fileDb.init();

const port = 8000;

app.use('/messages', messages);

app.listen(port, () => {
    console.log(`server port${port}`)
});