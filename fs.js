const fs = require('fs');
const {nanoid} = require("nanoid");

const path = './messages';
let data = [];

module.exports = {
    init() {
        try {
            if(!fs.existsSync(path)) {
                fs.mkdirSync(path);
            }
            fs.readdir(path, (err, files) => {
                files.forEach(file => {
                    const fileContent = fs.readFileSync(path + '/' + file);
                    const fileParsed = JSON.parse(fileContent);
                    data.push(fileParsed);
                })
            })
        } catch (e) {
            data = [];
        }
    },

    getItems() {
        const res = data.sort((a, b) => new Date(a.datetime) - new Date(b.datetime));
        if(data.length > 5) {
            const result = res.slice(Math.max(res.length - 5, 1)).reverse();
            return result;

        }
        return data;
    },

    addItem(item) {
        item.datetime = new Date();

        const fullPath = './' + path + '/' + nanoid() + '.txt';
        fs.writeFileSync(fullPath, JSON.stringify(item, null, 2));
        data.push(item);
    },

}